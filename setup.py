#!/usr/bin/env python

from setuptools import setup

setup(name='pyscreenman',
      version='0.2',
      description='Bluestone Python Screen Manager',
      author='Robert Seward',
      author_email='rseward@bluestone-consulting.com',
      url='http://www.bluestone-consulting.com/',
      # python package dirs will require at a minimum an empty __init__.py file
      #   in them.
      
      packages=['screenman'],
      scripts=['scripts/pyscreenlauncher'],
      package_dir={'screenman':'src/screenman'}
    )
