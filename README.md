Introduction
------------

This set of classes provide a means for controlling the well known screen utility from Python. 
Your python scripts may start screen sessions, open windows in the screen session and 
execute unix commands in the windows.

Theses utility classes are great for automating the startup of complex multi-process python systems
of programs. It is also a handy tool for automating the startup of involved screen sessions you may
use for development, server maintenance, or whatever.

Install
-------

pip install git+https://bitbucket.org/rtd/pyscreenman.git

Usage
-----

TBD

