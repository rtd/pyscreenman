#!/usr/bin/env python

from __future__ import print_function

import subprocess
import re
import time

SCREEN_CMD='/usr/bin/screen'

class ScreenWindow(object):
    def __init__(self, id, title, manager=None):
        self.id = id
        self.title = title
        self.manager = manager

    def sendCommand(self, command):
        """Paste text into a screen window."""

        if self.manager!=None:
            return self.manager.sendCommandToWindow( self, command)

        return False

    def sendCtrlC(self):
        """Send a ctrl-c to the window"""

        if self.manager!=None:
            return self.manager.sendCtrlCToWindow( self )

        return False
        

class ScreenManager(object):
    """Class for managing linux screen sessions."""

    def __init__(self, sessionname, maxwindows=10):
        self.sessionname=sessionname
        self._exists = self.exists()
        self.maxwindows = maxwindows
        self.windows = {}
        self.lastcommandts = None


    def start(self):
        """Start the screen session if it doesn't exist already."""

        if not(self.exists()):
            # Start the session because it doesn't exist yet.
            return 0 == self._start()

        return True

    def _start(self):
        """Unconditional start a screen session with the sessionname."""

        cmdlist=[SCREEN_CMD, '-S', self.sessionname, '-m', '-d', '/bin/bash']

        print( " ".join(cmdlist) )
        p = subprocess.Popen(cmdlist )
        ret = p.wait()

        for idx in range(0,self.maxwindows):
            self._allocateWindow()

        self.lastcommandts = time.time()

        self.wait(0.1)
        self._exists = self.exists()

        return ret

    def _allocateWindow(self):
        """Start a shell, so we have a predefined number of shells to work with."""

        cmdlist=[SCREEN_CMD, '-x', self.sessionname, '-X', 'screen'] # screen -dR curunir-lokal -X screen

        p = subprocess.Popen(cmdlist)
        return p.wait()

    def exists(self):
        """Determines if a session exists."""

        cmdlist=[ SCREEN_CMD, '-ls']
        p = subprocess.Popen(cmdlist, stdout=subprocess.PIPE )

        ret = p.wait()
        (pstdin, pstderr) = p.communicate()

        regex = re.compile( ".*%s.*" % ( self.sessionname ), flags=re.MULTILINE | re.DOTALL )

        self.pout = pstdin
        #print( pstdin )
        m = regex.match( pstdin.decode('utf-8') )
        if m !=None:
            return True

    def getWindow(self, title):
        return self.windows.get( title, None)

    def wait(self, delay):
        #print( f"wait {delay} secs..." )
        # wait until several seconds have passed since the last command
        if self.lastcommandts != None:
            now=time.time()
            while (now-self.lastcommandts<delay):
                time.sleep(0.1)
                now=time.time()
        
    def addWindow(self, window):

        #print( "addWindow _exists=%s" % self._exists )
        if self._exists:
            window.manager=self
            if not(window.title in self.windows ):
                # The windows doesn't exist yet. Let's create it.

                cmdlist=[SCREEN_CMD, '-x', self.sessionname, '-p', str(window.id), '-X', 'title', window.title]
                self.wait(0.2)

                print( " ".join(cmdlist) )
                p = subprocess.Popen(cmdlist)
                ret = p.wait()

                if ret == 0:
                    self.windows[ window.title ] = window

                self.lastcommandts = time.time()

                return ret == 0
        else:
            print( "Warning: session %s does not seem to exist" % self.sessionname )

        return False

    def sendCtrlCToWindow(self, window):
        ret = self._sendCmdToWindow( window, [ 'stuff', "%c" % 3] )
        return ret == 0

    def _sendCmdToWindow(self, window, subcmdlist):
        cmdlist=[SCREEN_CMD, '-x', self.sessionname, '-p', str(window.id), '-X']
        cmdlist.extend(subcmdlist)
        self.wait(0.2)

        p = subprocess.Popen( cmdlist )
        ret = p.wait()
        self.lastcommandts = time.time()
        return ret
        
    def sendCommandToWindow(self, window, command):
        with open( "screen.do", "w") as outf:
            outf.write( command )

        ret = self._sendCmdToWindow( window, ['readreg', 'p', 'screen.do'] )
        
        if ret == 0:
            # screen -dR $session -p $win -X paste p
            ret = self._sendCmdToWindow( window, ['paste', 'p' ] )

            return ret == 0

        return False

class ScreenLauncher(object):
    """Use a JSON object to launch a screen session."""

    def __init__(self, jsonscript):
        """@param jsonscript looks something like this:
 
           { "session" : 
              {
                "name" : "my-sessions-name",
                "numwindows" : 5,
                "windows" : [
                   { "name" : "java" ,
                     "command" : ". /user/rseward/curunir-aliases" },
                   { "name" : "jboss",
                     "command" : ". /user/rseward/curunir-aliases" }
                ]
             }
          }

        """

        import json
        self.json = json.loads( jsonscript )
        pass

    def launch(self):
        session = self.json.get( 'session', None )

        assert session != None, "session object does not exist in config!"

        name = session.get( "name", "screen-launch")
        nwin = session.get( "numwindows", 2 )

        self.screen = ScreenManager( name, nwin )
        self.screen.start()

        widx=0
        for w in session.get( "windows",  [] ):
            sw = ScreenWindow( widx,  w.get('name', 'win-%s' % widx) )

            self.screen.addWindow( sw )
            if w.get('command', None):
                sw.sendCommand( "%s\n" % w['command'] )
            widx=widx+1




        print( "screen session: '%s' started!" % session['name'] )
        print( "screen -dR %s" % session['name'] )

